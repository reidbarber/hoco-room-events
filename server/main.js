import '../imports/api/events.js';
import '../imports/api/requests.js';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Email } from 'meteor/email'
import { check } from 'meteor/check';
import '../imports/startup/env-config.js';

// Configurations for Meteor Accounts package
Accounts.config({

  // Restricts email accounts to only Ole Miss domain
  restrictCreationByEmailDomain: isEmailFromOleMiss = (email) => {
    const domainBeingChecked = email.slice(-11);
    const olemissdomain = 'olemiss.edu';
    return (domainBeingChecked === olemissdomain);
  }

});

// Publish user data to clients.
// For admins, publish all users
// For everyone else, publish only their own info
Meteor.publish("userData", function () {
    if(Meteor.user() && Meteor.user().role === 'admin') {
      return Meteor.users.find({});
    } else {
      return Meteor.users.find({ _id: this.userId});
    }
});

// Function is called on server when a user is being created.
Accounts.onCreateUser(function (options, user) {

    const admins = [
        "spolston@olemiss.edu",
        "letolber@olemiss.edu",
        "jsamonds@olemiss.edu",
        "dsg@olemiss.edu",
        "dbyoung@olemiss.edu",
        "jparsons@olemiss.edu",
        "pleeton@olemiss.edu",
    ];

    /*
     * Check if the user's email is an admin email
     * If not, no need to set a role
    */
    if (admins.includes(user.services.google.email)) {
      user.role = 'admin';
    }

    return user;
});
