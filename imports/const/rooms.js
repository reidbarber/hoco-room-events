const roomOptions = ([
   {
     text: 'Room 016',
     value: '016',
   },
   {
     text: 'Room 025',
     value: '025',
   },
   {
     text: 'Room 026',
     value: '026',
   },
   {
     text: 'Room 027',
     value: '027',
   },
   {
     text: 'Room 107',
     value: '107',
   },
   {
     text: 'Room 108',
     value: '108',
   },
   {
     text: 'Room 114',
     value: '114',
   },
   {
     text: 'Room 202',
     value: '202',
   },
   {
     text: 'Room 208',
     value: '208',
   },
   {
     text: 'Room 311',
     value: '311',
   },
   {
     text: 'Room 331',
     value: '331',
   }
 ]
);

export default roomOptions;
