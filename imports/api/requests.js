import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Events }  from './events.js';
export const Requests = new Mongo.Collection('requests');
import { Email } from 'meteor/email'


// Publish code only runs on the server
if (Meteor.isServer) {

  // Only publish event requests to admins
  Meteor.publish('requests', function requestPublication() {
    if(Meteor.user().role === 'admin') {
      return Requests.find({});
    } else {
      return null;
    }
  });


  Meteor.methods({
    'sendEmail'(to, from, subject, text) {
      // Make sure that all arguments are strings.
      check([to, from, subject, text], [String]);

      // Let other method calls from the same client start running, without
      // waiting for the email sending to complete.
      this.unblock();

      Email.send({ to, from, subject, text });
    }
  });





}




/*
    Methods for handling event requests
      - Create: For when non-admins make event requests.
                Request will enter requests collection until it has been approved or denied by an admin.
      - Approve: For admins to approve requests.
                  Adds request to events collections then deletes request from requests collection.
      - Deny: For admins to deny requests.
                  Removes request from request collection.
*/
Meteor.methods({
  'requests.create'(inputEvent) {

    // Validate input parameter type
    check(inputEvent, Object);

    // Make sure the user is logged in before inserting a event
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // Get user object so we can use their name and email in our request object.
    const user = Meteor.users.findOne(this.userId);

    // Insert request into requests collection
    Requests.insert({
      createdOn: new Date(),
      creatorName: user.services.google.name,
      creatorEmail: user.services.google.email,
      name: inputEvent.name,
      room: inputEvent.room,
      recurrence: inputEvent.recurrence,
      startDate: inputEvent.startDate,
      endDate: inputEvent.endDate,
      startTimeHours: inputEvent.startTimeHours,
      startTimeMins: inputEvent.startTimeMins,
      endTimeHours: inputEvent.endTimeHours,
      endTimeMins: inputEvent.endTimeMins
    });


  },


  'requests.approve'(requestId) {

    // Validate input parameter type
    check(requestId, String);

    // Get user object so we can check role and make sure they're an admin
    const user = Meteor.users.findOne(this.userId);

    // Make sure user is logged in and admin
    if (! this.userId || user.role != 'admin') {
      throw new Meteor.Error('not-authorized');
    }


    // Get request object from requests collection so we can use its values to insert new event.
    const request = Requests.findOne(requestId);

    // Insert event with same method admins use to create events.
    Meteor.call('events.create', {
      name: request.name,
      room: request.room,
      professor: request.creatorName,
      recurrence: request.recurrence,
      startDate: request.startDate,
      endDate: request.endDate,
      startTimeHours: request.startTimeHours,
      startTimeMins: request.startTimeMins,
      endTimeHours: request.endTimeHours,
      endTimeMins: request.endTimeMins
    });

    // Remove request from requests collection
    Requests.remove(requestId);

    Meteor.call(
      'sendEmail',
      `${request.creatorEmail}`,
      'smbhc-noreply@olemiss.edu',
      'Honors College Room Event',
      `Your event "${request.name}" has been approved and added to the calendar for Room ${request.room}.`
    );
  },



  'requests.deny'(requestId) {

    // Validate input parameter type
    check(requestId, String);

    // Get user object so we can check role and make sure they're an admin
    const user = Meteor.users.findOne(this.userId);

    // Make sure user is logged in and admin
    if (! this.userId || user.role != 'admin') {
      throw new Meteor.Error('not-authorized');
    }

    // Get request object from requests so we can use name and room for denial.
    const request = Object.assign({}, Requests.findOne(requestId));

    Requests.remove(requestId);

    Meteor.call(
      'sendEmail',
      `${request.creatorEmail}`,
      'smbhc-noreply@olemiss.edu',
      'Honors College Room Event',
      `Unfortunately, your event request for "${request.name}" in Room ${request.room} has been denied.`
    );

  },
});
