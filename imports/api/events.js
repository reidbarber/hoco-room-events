import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
var moment = require('moment');

export const Events = new Mongo.Collection('events');
export const Classes = new Mongo.Collection('classes');


// Publish code only runs on the server
if (Meteor.isServer) {

  // Publish all events to everyone
  Meteor.publish('events', function eventsPublication() {
    return Events.find({});
  });

  // Publish all classes to everyone
  Meteor.publish('classes', function classesPublication() {
    return Classes.find({});
  });
}




/*
    Methods for handling events
      - Create: For when admins create events
      - Delete: For when admins delete events
*/
Meteor.methods({
  'events.create'(inputEvent) {

    // Validate input parameter type
    check(inputEvent, Object);

    // Get user object so we can check role and make sure they're an admin
    const user = Meteor.users.findOne(this.userId);

    // Make sure user is logged in and admin
    if (! this.userId || user.role != 'admin') {
      throw new Meteor.Error('not-authorized');
    }

    // Insert class to classes collection
    const currentClass = Classes.insert({
      name: inputEvent.name,
      room: inputEvent.room,
      professor: inputEvent.professor,
      recurrence: inputEvent.recurrence,
      startDate: inputEvent.startDate,
      endDate: inputEvent.endDate,
      startTimeHours: inputEvent.startTimeHours,
      startTimeMins: inputEvent.startTimeMins,
      endTimeHours: inputEvent.endTimeHours,
      endTimeMins: inputEvent.endTimeMins
    });


    // Convert date objects from Date object to Moment objects.
    //
    // The Moment library is very useful for us on both the front-end and back-end,
    // however Moment objects aren't serializable to EJSON, so we need to convert them
    // to Date objects on the front-end before sending them over. Here, we are converting
    // them back to we can leverage the power of Moment. We'll also need to convert them back
    // before inserting them into the database.
    var currentDate = moment(inputEvent.startDate);
    const endDate = moment(inputEvent.endDate);

    // Given the start and end date, we want to test each day in between, and increment after each test.
    while (currentDate <= endDate){

      // If the current day (e.g. Monday = 0) is included in the recurrence
      // we want to add an event for this day. Otherwise, skip this day.
      if(inputEvent.recurrence.includes(currentDate.day())){

        // New start/end times where we add hours and minutes to a copy for the current day.
        // After converting back to Date objects, we have our times that will be inserted
        // into the databse.
        //
        // We use clone() because Moment objects are mutable. We'll want to reuse currentDate so let's not change it any when adding.
        // We use add() to add the hours (subtract by 12 since its 24hr time) and minutes to the day since the day starts at 00:00.
        // We use toDate() to convert back to a Date object to store in the databse.
        const currentStartTime = currentDate.clone().add({ hours: inputEvent.startTimeHours-12, minutes: inputEvent.startTimeMins }).toDate();
        const currentEndTime = currentDate.clone().add({ hours: inputEvent.endTimeHours-12, minutes: inputEvent.endTimeMins }).toDate();


        // Add event to events collection
        Events.insert({
          class: currentClass,
          name: inputEvent.name,
          room: inputEvent.room,
          professor: inputEvent.professor,
          startTime: currentStartTime,
          endTime: currentEndTime,
        });
      }

      // Increment day by one so we can try next day.
      currentDate.add(1, 'day');
    }
  },

  'events.remove'(eventClass) {

    // Validate input parameter type
    check(eventClass, String);

    // Get user object so we can check role and make sure they're an admin
    const user = Meteor.users.findOne(this.userId);

    // Make sure user is logged in and admin
    if (! this.userId || user.role != 'admin') {
      throw new Meteor.Error('not-authorized');
    }

    // Remove event from events collection
    Events.remove({class: eventClass});

    // Remove class from classes collection
    Classes.remove(eventClass);
  },

  'events.removeOne'(eventID, eventClass) {

    // Validate input parameter type
    check(eventID, String);

    // Get user object so we can check role and make sure they're an admin
    const user = Meteor.users.findOne(this.userId);

    // Make sure user is logged in and admin
    if (! this.userId || user.role != 'admin') {
      throw new Meteor.Error('not-authorized');
    }

    // Remove event from events collection
    Events.remove(eventID);

    // If there are no more events left from a class, we can remove the class too.
    const eventCount = Events.find({class: eventClass}).count();
    if(eventCount === 0){
      Classes.remove(eventClass);
    }
  }
});
