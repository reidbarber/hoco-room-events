var oAuth = Meteor.settings.private.oAuth;
var environment = process.env.NODE_ENV;


ServiceConfiguration.configurations.upsert({
  service: "google"
}, {
  $set: {
    clientId: environment === "production" ? oAuth.google.production.clientId : oAuth.google.development.clientId,
    loginStyle: "popup",
    secret: environment === "production" ? oAuth.google.production.secret : oAuth.google.development.secret
  }
});
