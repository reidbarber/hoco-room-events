# Honors College Room Event Web Application

Web application for managing room schedules in The Sally McDonnell Barksdale Honors College at The University of Mississippi

## Installing

1. Install [Meteor](https://www.meteor.com/install)

2. Clone and install this project

```sh
git clone git@gitlab.com:reidbarber/hoco-room-events.git
cd hoco-room-events
meteor npm install
```

## Configuring Google OAuth API keys

1. Create a file called **settings.json** in your hoco-room-events folder and add the following to it:
```json
{
	"private": {
		"oAuth": {
			"google": {
                "development": {
                    "clientId": "xxXXxx",
                    "secret": "xxXXxx"
                }
            }
		}
    }
}
```

2. Obtain a Google OAuth ClientID and Secret from the [Google Developer Console](https://console.developers.google.com/)
    * Create Project (if needed)
    * Navigate to "Credentials"
    * Navigate to "OAuth consent screen" and enter an email address and product name, then save.
    * Navigate to "Credentials" and select "Create Credentials" then "OAuth client ID" then "Web application".
    * Set "Authorized JavaScript origins" to `http://localhost:3000` and set Authorized redirect URIs" to `http://localhost:3000/_oauth/google` then select "Create". 
3. Paste your clientId and secret into the settings.json file created earlier.

## Running

```sh
meteor --settings settings.json
```

The app should start running at [http://localhost:3000/](http://localhost:3000/)
