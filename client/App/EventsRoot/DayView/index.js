import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Dropdown, Segment, Rail, Grid, Header, Popup } from 'semantic-ui-react';
var moment = require('moment');
import { Events } from '../../../../imports/api/events.js';
import roomOptions from '../../../../imports/const/rooms.js';
import Event from '../WeekView/Event';

// Implementing hours as an array just to save space.
const hours = ([
  '8AM', '9AM', '10AM', '11AM', '12AM', '1PM', '2PM', '3PM', '4PM', '5PM'
]);

class DayView extends Component {
  constructor(props) {
    super(props);
  }


  /*  Function for adding events from array to organized object.
      By having my own object, I can better render the state,
      based on rooms in each column for instance.
  */
  parseEvents(oldEvents) {
    const newEvents = [];

    /*  For each room, make a new object in new array
        It should look like this when done:
          [ {name: '016', events: [] },
            {name: '017', events: [] }... ]
    */
    roomOptions.forEach(function(room){
      newEvents.push({name: room.value, events: [] });
    });

    // For each event, add to correct room
    oldEvents.forEach(function(event){
      // Adds event to respective day in new object
      newEvents[newEvents.findIndex( obj => obj.name === event.room )].events.push(event);
    });

    // Return newly populated object
    return newEvents;
  }

  render() {
    const events = this.parseEvents(this.props.events);
    return (
        <div style={{height: '100%'}}>

          {/* Each Room:  Prints each room across top */}
          <Segment.Group horizontal style={{margin: 0, border: 'none', boxShadow: 'none'}}>
            <Segment textAlign='center' style={{width: '4%', height: '100%'}}></Segment>
            {events.map((room, index) =>
              <Segment textAlign='center' style={{width: '7.833%', height: '100%', border: 'none'}} key={index}>{room.name}</Segment>
            )}
          </Segment.Group>

          <Segment.Group style={{height: 'calc(100% - 50px)', margin: 0, border: 'none'}}>
            <Segment.Group horizontal style={{height: '100%'}}>

              {/* Times Column:    Prints each hours from hour array. */}
              <Segment textAlign='center' style={{width: '4%',height: '100%', padding: 0}}>
                <Segment.Group horizontal style={{height: '100%', margin: 0, border: 'none'}}>
                  <table style={{height: '100%', width: '100%'}}>
                    <tbody>
                      {hours.map((hours, index) =>
                        <tr key={index} style={{verticalAlign: 'top', textAlign: 'right'}}>
                          <td style={{paddingRight: 5}}>{hours}</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </Segment.Group>
              </Segment>

              {/* Each column (1 per room) */}
              {events.map((room, index) =>
                <Segment textAlign='center' style={{width: '8.7272%',height: '100%', padding: 0}} key={index}>

                  {/* Prints grey bars each hour down column */}
                  <table style={{height: '100%', width: '100%'}}>
                    <tbody>
                      {hours.map((hours, index) =>
                        <tr key={index} style={{verticalAlign: 'top'}}>
                          <td style={{boxShadow: '0 1px 0 #ddd'}}></td>
                        </tr>
                      )}
                    </tbody>
                  </table>


                  {/* Each event in a room */}
                  {room.events.map((event, index) =>
                    <Event
                      key={event._id}
                      event={event}
                    />
                  )}
                </Segment>
            )}
            </Segment.Group>
          </Segment.Group>
        </div>
    );
  }
}


export default withTracker(props => {
  const date = props.date.clone();
  const results = Meteor.subscribe('events');

  /*
    Here is our query. We want to return all events that:
      1. Start after the beginning of the week of the selected date.
        AND
      2. Start before the end of the week of the selected date.

      (i.e. they can be from any room)
  */
  return {
    events: Events.find({
      $and: [
        {startTime: {$gte: date.startOf('day').toDate()}},
        {startTime: {$lte: date.endOf('day').toDate()}}
      ]
    }).fetch(),
    loading: !results.ready(),
    currentUser: Meteor.user(),
  };
})(DayView);
