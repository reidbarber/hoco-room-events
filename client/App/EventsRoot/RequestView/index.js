import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Modal, Button, Form, Icon, Dropdown, Message } from 'semantic-ui-react';
import { DateRangePicker } from 'react-dates';
import roomOptions from '../../../../imports/const/rooms.js';
var moment = require('moment');


const labelStyle = {
  display: 'block',
  margin: '0 0 .28571429rem 0',
  color: 'rgba(0,0,0,.87)',
  fontSize: '.92857143em',
  fontWeight: '700',
  textTransform: 'none',
}


class RequestView extends Component {
  constructor(props) {
    super(props);
    this.state={
      stagingEvent: {
        name: '',
        room: roomOptions[0].value,
        recurrence: [],
        startDate: null,
        endDate: null,
        startTime: "08:00",
        endTime: "08:50"
      },
      focusedInput: null,
      modalOpen: false
    }
  }


    handleChangeField = (e, { name, value }) => {
      this.setState({ stagingEvent: { ...this.state.stagingEvent, [name]: value} });
    }

    handleChangeRecurrence = (e, { name, value }) => {
      const num = parseInt(name);
      if(this.state.stagingEvent.recurrence.includes(num)){
        this.setState({ stagingEvent: { ...this.state.stagingEvent, recurrence: this.state.stagingEvent.recurrence.filter(val => val != num)} });
      } else {
        this.setState({ stagingEvent: { ...this.state.stagingEvent, recurrence: this.state.stagingEvent.recurrence.concat(num)} });
      }
    }

    submitEvent = () => {
      console.log(this.state.stagingEvent);
      const startTimeHours = parseInt(this.state.stagingEvent.startTime.slice(0,2), 10);
      const startTimeMins = parseInt(this.state.stagingEvent.startTime.slice(-2), 10);
      const endTimeHours = parseInt(this.state.stagingEvent.endTime.slice(0,2), 10);
      const endTimeMins = parseInt(this.state.stagingEvent.endTime.slice(-2), 10);
      Meteor.call('requests.create', {
        name: this.state.stagingEvent.name,
        room: this.state.stagingEvent.room,
        recurrence: this.state.stagingEvent.recurrence,
        startDate: this.state.stagingEvent.startDate.toDate(),
        endDate: this.state.stagingEvent.endDate.toDate(),
        startTimeHours,
        startTimeMins,
        endTimeHours,
        endTimeMins
      });

      this.handleClose();
      this.setState({stagingEvent: {
        name: '',
        room: roomOptions[0].value,
        recurrence: [],
        startDate: null,
        endDate: null,
        startTime: "08:00",
        endTime: "08:50"
      }})

      this.props.openMessage("Your event request was sent!", "You\'ll get an email when it is either confirmed or denied.");

    }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })


  render() {
    const event = this.state.stagingEvent;

    return (
      <Modal open={this.state.modalOpen} onClose={this.handleClose} trigger={<Button icon labelPosition='left' onClick={this.handleOpen}><Icon name='add to calendar' />Request Room</Button>} closeOnDimmerClick={false} closeIcon>
          <Modal.Header>Request Room</Modal.Header>
          <Modal.Content>
            <Form>
              <div style={{width: '50%', display: 'inline-block', float: 'left', paddingLeft: '.5em', paddingRight: '.5em'}}>
              <Form.Group unstackable widths={2}>
                <Form.Input label='Event Name' name="name" value={event.name} onChange={this.handleChangeField} />
              </Form.Group>

              <div>
                <label style={labelStyle}>Room</label>
                <Dropdown placeholder='Select Room' name="room" scrolling selection options={roomOptions} value={this.state.stagingEvent.room} onChange={this.handleChangeField} style={{margin: 'auto', maxWidth: 200}}/>
              </div>

              <div style={{paddingTop: 20}}>
                <label style={labelStyle}>Times</label>
                <div style={{margin: 'auto'}}>
                  <input
                    id="startTime"
                    type="time"
                    value={this.state.stagingEvent.startTime}
                    onChange={(event) => this.setState({ stagingEvent: { ...this.state.stagingEvent, startTime: event.target.value} })}
                    style={{width: 130, marginRight: 24}}
                  />
                  <input
                    id="endTime"
                    type="time"
                    value={this.state.stagingEvent.endTime}
                    onChange={(event) => this.setState({ stagingEvent: { ...this.state.stagingEvent, endTime: event.target.value} })}
                    style={{width: 130}}
                  />
                </div>
              </div>
            </div>

              <div style={{width: '50%', display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em'}}>

                <div>
                  <label style={labelStyle}>Date Range</label>
                  <div style={{textAlign: 'center', margin: '15px auto'}}>
                    <DateRangePicker
                      startDate={this.state.stagingEvent.startDate}
                      startDateId="your_unique_start_date_id"
                      endDate={this.state.stagingEvent.endDate}
                      endDateId="your_unique_end_date_id"
                      onDatesChange={({ startDate, endDate }) => this.setState({ stagingEvent: { ...this.state.stagingEvent, startDate, endDate } })}
                      focusedInput={this.state.focusedInput}
                      onFocusChange={focusedInput => this.setState({ focusedInput })}
                      isOutsideRange={() => false}
                    />
                  </div>
                </div>

                <div>
                  <label style={labelStyle}>Days of Week</label>
                  <div style={{paddingTop: 5}}>
                    <Form.Checkbox label='Sunday' checked={event.recurrence.includes(0)} name="0" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Monday' checked={event.recurrence.includes(1)} name="1" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Tuesday' checked={event.recurrence.includes(2)} name="2" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Wednesday' checked={event.recurrence.includes(3)} name="3" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Thursday' checked={event.recurrence.includes(4)} name="4" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Friday' checked={event.recurrence.includes(5)} name="5" onChange={this.handleChangeRecurrence} />
                    <Form.Checkbox label='Saturday' checked={event.recurrence.includes(6)} name="6" onChange={this.handleChangeRecurrence} />
                  </div>
                </div>
              </div>

              <div style={{paddingTop: 20}}>
                <Button style={{display: 'block', margin: '25px auto'}}
                  type='submit'
                  onClick={this.submitEvent}
                  disabled={this.state.stagingEvent.name.length > 0 && this.state.stagingEvent.startDate != null && this.state.stagingEvent.endDate != null ? false : true}
                  >
                  Request
                </Button>
              </div>
            </Form>
          </Modal.Content>
        </Modal>
    );
  }
}

export default RequestView;
