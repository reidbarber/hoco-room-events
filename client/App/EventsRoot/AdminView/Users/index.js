import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Table } from 'semantic-ui-react';
import { Events } from '../../../../../imports/api/events.js';
import { withTracker } from 'meteor/react-meteor-data';


class Users extends Component {
  constructor(props) {
    super(props);
    this.state={
    }
  }

  render() {
    const users = this.props.users;
    const loading = this.state.loading;
    return (
      <Table compact style={{height: '100%', display: 'block', overflowY: 'auto'}}>
        <Table.Header style={{position: 'sticky', display: 'table', width: '100%', top: 0, tableLayout: 'fixed'}}>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Role</Table.HeaderCell>
            <Table.HeaderCell>Created On</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body style={{display: 'table', width: '100%', tableLayout: 'fixed'}}>
          {users.map((user, index) => (
              <Table.Row key={index}>
                <Table.Cell>{user.services.google.name}</Table.Cell>
                <Table.Cell>{user.services.google.email}</Table.Cell>
                <Table.Cell>{user.role ? user.role : ''}</Table.Cell>
                <Table.Cell>{user.createdAt.toLocaleDateString("en-US",{ year: 'numeric', month: 'numeric', day: 'numeric' })}</Table.Cell>
              </Table.Row>
            )
          )}
        </Table.Body>
      </Table>
    );
  }
}

export default withTracker(() => {
  const subscription = Meteor.subscribe('users');

  return {
    users: Meteor.users.find({}).fetch(),
    loading: !subscription.ready()
  };
})(Users);
