import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Table, Button, Icon } from 'semantic-ui-react';
import { Requests } from '../../../../../imports/api/requests.js';
import { withTracker } from 'meteor/react-meteor-data';
var moment = require('moment');


class EventRequests extends Component {
  constructor(props) {
    super(props);
    this.state={
    }
  }

  handleApprove = (requestId) => {
    Meteor.call('requests.approve', requestId);
  }

  handleDeny = (requestId) => {
    Meteor.call('requests.deny', requestId);
  }

  renderRecurrenceText(inputArray){
    var dayArray = [];
    inputArray.forEach(function(item){
      switch (item) {
        case 0:
            dayArray.push('Sun');
            break;
        case 1:
            dayArray.push('M');
            break;
        case 2:
            dayArray.push('Tu');
            break;
        case 3:
            dayArray.push('W');
            break;
        case 4:
            dayArray.push('Th');
            break;
        case 5:
            dayArray.push('F');
            break;
        case 6:
            dayArray.push('Sat');
            break;
        default:
            break;
      }
    });
    return dayArray.join(", ");
  }

  render() {
    const requests = this.props.requests;
    const loading = this.state.loading;
    return (

      <Table compact style={{height: '100%', display: 'block', overflowY: 'auto'}}>
        <Table.Header style={{position: 'sticky', display: 'table', width: '100%', top: 0, tableLayout: 'fixed'}}>
          <Table.Row>
            <Table.HeaderCell>Submitted By</Table.HeaderCell>
            <Table.HeaderCell>Event Name</Table.HeaderCell>
            <Table.HeaderCell>Room</Table.HeaderCell>
            <Table.HeaderCell>Dates</Table.HeaderCell>
            <Table.HeaderCell>Time</Table.HeaderCell>
            <Table.HeaderCell>Days</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body style={{display: 'table', width: '100%', tableLayout: 'fixed'}}>
          {requests.map((event, index) => (
              <Table.Row key={index}>
                <Table.Cell>{event.creatorName}</Table.Cell>
                <Table.Cell>{event.name}</Table.Cell>
                <Table.Cell>{event.room}</Table.Cell>
                <Table.Cell>{moment(event.startDate).format('MM/DD/YY')}- <br /> {moment(event.endDate).format('MM/DD/YY')}</Table.Cell>
                <Table.Cell>{moment(event.startTimeHours + ":" + event.startTimeMins, "H:mm").format('h:mmA')}- <br /> {moment(event.endTimeHours + ":" + event.endTimeMins, "H:mm").format('h:mmA')}</Table.Cell>
                <Table.Cell>{this.renderRecurrenceText(event.recurrence)}</Table.Cell>
                <Table.Cell>
                  <div style={{marginLeft: '-50px'}}>
                    <Button.Group>
                      <Button icon href={`mailto:${event.creatorEmail}`}>
                        <Icon name='mail' />
                      </Button>
                      <Button negative style={{fontSize: '.8rem', padding: 10}} size='tiny' onClick={() => this.handleDeny(event._id)}>Deny</Button>
                      <Button positive style={{fontSize: '.8rem', padding: 10}} size='tiny' onClick={() => this.handleApprove(event._id)}>Approve</Button>
                    </Button.Group>
                  </div>
                </Table.Cell>
              </Table.Row>
            )
          )}
        </Table.Body>

      </Table>

    );
  }
}

export default withTracker(() => {
  const subscription = Meteor.subscribe('requests');

  return {
    requests: Requests.find({}).fetch(),
    loading: !subscription.ready()
  };
})(EventRequests);
