import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Modal, Button, Form, Dropdown, Tab } from 'semantic-ui-react';
var moment = require('moment');
import { Events } from '../../../../../imports/api/events.js';
import { DateRangePicker } from 'react-dates';
import roomOptions from '../../../../../imports/const/rooms.js';


const labelStyle = {
  display: 'block',
  margin: '0 0 .28571429rem 0',
  color: 'rgba(0,0,0,.87)',
  fontSize: '.92857143em',
  fontWeight: '700',
  textTransform: 'none',
}

class AddEvents extends Component {
  constructor(props) {
    super(props);
    this.state={
      stagingEvent: {
        name: '',
        room: roomOptions[0].value,
        professor: '',
        recurrence: [],
        startDate: null,
        endDate: null,
        startTime: "08:00",
        endTime: "08:50"
      },
      focusedInput: null
    }
  }

  handleChangeField = (e, { name, value }) => {
    this.setState({ stagingEvent: { ...this.state.stagingEvent, [name]: value} });
  }

  handleChangeRecurrence = (e, { name, value }) => {
    const num = parseInt(name);
    if(this.state.stagingEvent.recurrence.includes(num)){
      this.setState({ stagingEvent: { ...this.state.stagingEvent, recurrence: this.state.stagingEvent.recurrence.filter(val => val != num)} });
    } else {
      this.setState({ stagingEvent: { ...this.state.stagingEvent, recurrence: this.state.stagingEvent.recurrence.concat(num)} });
    }
  }

  submitEvent = () => {
    console.log(this.state.stagingEvent);
    const startTimeHours = parseInt(this.state.stagingEvent.startTime.slice(0,2), 10);
    const startTimeMins = parseInt(this.state.stagingEvent.startTime.slice(-2), 10);
    const endTimeHours = parseInt(this.state.stagingEvent.endTime.slice(0,2), 10);
    const endTimeMins = parseInt(this.state.stagingEvent.endTime.slice(-2), 10);
    Meteor.call('events.create', {
      name: this.state.stagingEvent.name,
      room: this.state.stagingEvent.room,
      professor: this.state.stagingEvent.professor,
      recurrence: this.state.stagingEvent.recurrence,
      startDate: this.state.stagingEvent.startDate.toDate(),
      endDate: this.state.stagingEvent.endDate.toDate(),
      startTimeHours,
      startTimeMins,
      endTimeHours,
      endTimeMins
    });

    this.props.openMessage("Event successfully added!", "Added " + this.state.stagingEvent.name + " to the schedule for Room " + this.state.stagingEvent.room + ".");

  }






  render() {
    const event = this.state.stagingEvent;
    return (

            <Form>
              <Form.Group unstackable widths={2}>
                <Form.Input label='Event Name' name="name" value={event.name} onChange={this.handleChangeField} />
                <Form.Input label='Professor (Optional)' name="professor" value={event.professor} onChange={this.handleChangeField}/>
              </Form.Group>

              <div>
                <div style={{width: '50%', display: 'inline-block', float: 'right', paddingLeft: '.5em', paddingRight: '.5em'}}>

                  <div>
                    <label style={labelStyle}>Date Range</label>
                    <div style={{textAlign: 'center', margin: '15px auto'}}>
                      <DateRangePicker
                        startDate={this.state.stagingEvent.startDate}
                        startDateId="your_unique_start_date_id"
                        endDate={this.state.stagingEvent.endDate}
                        endDateId="your_unique_end_date_id"
                        onDatesChange={({ startDate, endDate }) => this.setState({ stagingEvent: { ...this.state.stagingEvent, startDate, endDate } })}
                        focusedInput={this.state.focusedInput}
                        onFocusChange={focusedInput => this.setState({ focusedInput })}
                        isOutsideRange={() => false}
                      />
                    </div>
                  </div>


                  <label style={labelStyle}>Room</label>
                  <Dropdown placeholder='Select Room' name="room" scrolling selection options={roomOptions} value={this.state.stagingEvent.room} onChange={this.handleChangeField} style={{display: 'block', margin: '15px auto', maxWidth: 200}}/>

                  <div style={{paddingBottom: 20}}>
                    <label style={labelStyle}>Times</label>
                    <div style={{textAlign: 'center', margin: '15px auto'}}>
                      <input
                        id="startTime"
                        type="time"
                        value={this.state.stagingEvent.startTime}
                        onChange={(event) => this.setState({ stagingEvent: { ...this.state.stagingEvent, startTime: event.target.value} })}
                        style={{width: 130, marginRight: 24}}
                      />
                      <input
                        id="endTime"
                        type="time"
                        value={this.state.stagingEvent.endTime}
                        onChange={(event) => this.setState({ stagingEvent: { ...this.state.stagingEvent, endTime: event.target.value} })}
                        style={{width: 130}}
                      />
                    </div>
                  </div>
                </div>



                <div style={{width: '50%', display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em'}}>
                  <label style={labelStyle}>Days of Week</label>
                  <Form.Checkbox label='Sunday' checked={event.recurrence.includes(0)} name="0" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Monday' checked={event.recurrence.includes(1)} name="1" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Tuesday' checked={event.recurrence.includes(2)} name="2" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Wednesday' checked={event.recurrence.includes(3)} name="3" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Thursday' checked={event.recurrence.includes(4)} name="4" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Friday' checked={event.recurrence.includes(5)} name="5" onChange={this.handleChangeRecurrence} />
                  <Form.Checkbox label='Saturday' checked={event.recurrence.includes(6)} name="6" onChange={this.handleChangeRecurrence} />
                </div>
              </div>

              <div style={{marginTop: 50}}>
                <Button style={{display: 'block', margin: '25px auto'}}
                  type='submit'
                  disabled={this.state.stagingEvent.name.length > 0 && this.state.stagingEvent.startDate != null && this.state.stagingEvent.endDate != null ? false : true}
                  onClick={this.submitEvent}
                  >
                    Create
                  </Button>
              </div>
            </Form>
    );
  }
}

export default AddEvents;
