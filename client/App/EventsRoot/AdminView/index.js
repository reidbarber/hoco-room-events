import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Modal, Button, Tab, Icon } from 'semantic-ui-react';
import AddEvents from './AddEvents';
import Users from './Users';
import EventRequests from './EventRequests';
import RemoveEvents from './RemoveEvents';


class AdminView extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <Modal size="large" trigger={<Button icon labelPosition='left'><Icon name='setting' />Admin Tools</Button>} closeOnDimmerClick={false} closeIcon>
          <Modal.Header>Admin Tools</Modal.Header>
          <Modal.Content>
            <Tab panes={[
            {
              menuItem: { key: 'add', icon: 'add to calendar', content: 'Add Events' },
              render: () => <Tab.Pane style={{height: '60vh'}}><AddEvents openMessage={this.props.openMessage}/></Tab.Pane>,
            },
            {
              menuItem: { key: 'remove', icon: 'delete calendar', content: 'Remove Events' },
              render: () => <Tab.Pane style={{height: '60vh'}}><RemoveEvents openMessage={this.props.openMessage}/></Tab.Pane>,
            },
            {
              menuItem: { key: 'pending', icon: 'wait', content: 'Event Requests' },
              render: () => <Tab.Pane style={{height: '60vh'}}><EventRequests /></Tab.Pane>,
            },
            {
              menuItem: { key: 'users', icon: 'users', content: 'Users' },
              render: () => <Tab.Pane style={{height: '60vh'}}><Users /></Tab.Pane>,
            }
            ]} />
          </Modal.Content>
        </Modal>
    );
  }
}

export default AdminView;
