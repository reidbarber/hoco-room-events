import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import AccountsUIWrapper from '../AccountsUIWrapper.js';
import WeekView from './WeekView';
import DayView from './DayView';
import AdminView from './AdminView';
import RequestView from './RequestView';
import { Dropdown, Segment, Rail, Grid, Header, Popup, Tab, Button, Menu, Message, Transition } from 'semantic-ui-react';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { SingleDatePicker } from 'react-dates';
import roomOptions from '../../../imports/const/rooms.js';
var moment = require('moment');
import { withTracker } from 'meteor/react-meteor-data';



class EventsRoot extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedDate: moment(), selectedRoom: roomOptions[0].value, focused: null, view: 0, messageOpen: false, message: {header: '', content: ''}};
  }

  // Function for changing selected room in state.
  // Triggered by using room dropdown menu
  handleChangeRoom = (event, value) => {
    this.setState({ selectedRoom: value.value });
  };

  // Function for changing selected date
  // Triggered by using react-dates picker
  handleChangeDate = selectedDate => {
    this.setState({ selectedDate });
  };

  // Function for changing between day and week view.
  // Triggered by selecting Day or Week buttons at top
  handleChangeView = (e, { view }) => {
    this.setState({ view })
  };

  openMessage = (header, content) => {
    this.setState({messageOpen: true, message: {header, content}});

    setTimeout(() => {
      this.setState({ messageOpen: false });
    }, 3000);
  }

  render() {
    const date = this.state.selectedDate;
    const room = this.state.selectedRoom;
    const view = this.state.view;
    let currentUser = this.props.currentUser;
    return (
        <div style={{height: '100vh'}}>

          {/* Accounts UI, which is hidden, still needs to be on the page though. */}
          <AccountsUIWrapper />

          {/* Top Menu */}
          <Menu size='small' style={{margin: 0, height: '7%'}}>
            <div style={{margin: 'auto 10px', display: 'inline-block'}}>
              {/* Date Picker */}
              <SingleDatePicker
                date={date}
                onDateChange={this.handleChangeDate}
                focused={this.state.focused}
                onFocusChange={({ focused }) => this.setState({ focused })}
                showDefaultInputIcon
                numberOfMonths={1}
                isOutsideRange={() => false}
              />
            </div>

            {/* Room Dropdown */}
            <Dropdown style={{margin: 'auto 10px'}} placeholder='Select Room' disabled={view === 0 ? false : true } scrolling selection options={roomOptions} value={this.state.selectedRoom} onChange={this.handleChangeRoom}/>

            {/* Day/Week View Changer */}
            <div style={{display: 'block', margin: 'auto', height: '100%'}}>
              <Menu secondary style={{height: '100%'}}>
                <Menu.Item name='Week View' active={view === 0} onClick={this.handleChangeView} view={0} style={{height: '100%', margin: 0}}/>
                <Menu.Item name='Day View' active={view === 1} onClick={this.handleChangeView} view={1} style={{height: '100%', margin: 0}}/>
              </Menu>
            </div>

            {/* Right floating menu items */}
            <Menu.Menu position='right'>

              {/* If user is logged in, display admin options if admin, or request option if not */}
              { currentUser ?
                <div style={{display: 'inline-block', margin: 'auto 10px'}}>

                  { currentUser.role && currentUser.role === 'admin' &&
                    <AdminView openMessage={this.openMessage} />
                  }

                  <RequestView openMessage={this.openMessage} />
                  <Dropdown item text={currentUser.services ? currentUser.services.google.name : ''} style={{display: 'inline-block', margin: 'auto 10px'}}>
                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => { document.getElementById('login-buttons-logout').click() }}>Sign out</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                    :
                <Menu.Item>
                  <Button primary onClick={() => { document.getElementById('login-buttons-google').click() }}>Sign In</Button>
                </Menu.Item>
              }
            </Menu.Menu>


          </Menu>


          {/* Week/Day View */}
          <Segment style={{padding: 0, margin: 0, height: '93%', border: 'none', boxShadow: 'none'}}>

            {/* Either week or day view renders, based on state. */}
            { view == 0 ?
              <WeekView date={date} room={room} />
              :
              <DayView date={date} />
            }

          </Segment>

          <Transition visible={this.state.messageOpen} animation='scale' duration={500}>
            <Message
              success
              header={this.state.message.header}
              content={this.state.message.content}
              style={{position: 'absolute', top: 60, right: 10, zIndex: 10000}}
            />
          </Transition>

        </div>
    );
  }
}

export default withTracker(() => {
  // Subscribe to user data, so we can know user's name
  Meteor.subscribe("userData");

  // Get user information from database if not logged in.
  return {
    currentUser: Meteor.user(),
  };
})(EventsRoot);
