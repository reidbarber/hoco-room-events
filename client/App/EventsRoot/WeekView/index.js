import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Dropdown, Segment, Rail, Grid, Header, Popup } from 'semantic-ui-react';
var moment = require('moment');
import { Events } from '../../../../imports/api/events.js';
import Event from './Event';

// Implementing hours as an array just to save space.
const hours = ([
  '8AM', '9AM', '10AM', '11AM', '12AM', '1PM', '2PM', '3PM', '4PM', '5PM'
]);

class WeekView extends Component {
  constructor(props) {
    super(props);
  }

  /*  Function for adding events from array to organized object.
      By having my own object, I can better render the state,
      based on days in each column for instance.
  */
  parseEvents(oldEvents) {
    const newEvents = [
      {name: 'Sunday', events: [] },
      {name: 'Monday', events: [] },
      {name: 'Tuesday', events: [] },
      {name: 'Wednesday', events: [] },
      {name: 'Thursday', events: [] },
      {name: 'Friday', events: [] },
      {name: 'Saturday', events: [] }
    ];

    // Add each event form array to the correct day in object.
    oldEvents.forEach(function(event){
      // Adds event to respective day in new object
      newEvents[event.startTime.getDay()].events.push(event);
    });

    // Return populated object object
    return newEvents;
  }

  render() {
    const events = this.parseEvents(this.props.events);
    return (
        <div style={{height: '100%'}}>
          {/* Days of Week:    Prints each day of week across top */}
          <Segment.Group horizontal style={{margin: 0, border: 'none', boxShadow: 'none'}}>
            <Segment textAlign='center' style={{width: '4%', height: '100%'}}></Segment>
            {events.map((day, index) =>
              <Segment textAlign='center' style={{width: '13.714%', height: '100%', border: 'none'}} key={index}>{day.name}</Segment>
            )}
          </Segment.Group>

          <Segment.Group style={{height: 'calc(100% - 50px)', margin: 0, border: 'none'}}>
            <Segment.Group horizontal style={{height: '100%'}}>

              {/* Times Column :    Prints each hours from hour array. */}
              <Segment textAlign='center' style={{width: '4%',height: '100%', padding: 0}}>
                <Segment.Group horizontal style={{height: '100%', margin: 0, border: 'none'}}>
                  <table style={{height: '100%', width: '100%'}}>
                    <tbody>
                      {hours.map((hours, index) =>
                        <tr key={index} style={{verticalAlign: 'top', textAlign: 'right'}}>
                          <td style={{paddingRight: 5}}>{hours}</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </Segment.Group>
              </Segment>

              {/* Each column (1 per week day) */}
              {events.map((day, index) =>
                <Segment textAlign='center' style={{width: '13.714%',height: '100%', padding: 0, backgroundColor: this.props.date.toDate().getDay() === index ? 'rgb(221, 221, 221, 0.3)' : 'initial'}} key={index}>

                  {/* Prints grey bars each hour down column */}
                  <table style={{height: '100%', width: '100%'}}>
                    <tbody>
                      {hours.map((hours, index) =>
                        <tr key={index} style={{verticalAlign: 'top'}}>
                          <td style={{boxShadow: '0 1px 0 #ddd'}}></td>
                        </tr>
                      )}
                    </tbody>
                  </table>

                  {
                    /*  If current date is present, print a special marker at current time of day
                    *   Also, percent must be less than 100 (current time < 6 oclock)
                    */
                    moment().isSame(moment(this.props.date), 'week') && moment().weekday() === index && (((new Date().getHours() + (new Date().getMinutes() / 60))-8)*10) < 100 &&
                    <div style={{ width: '100%', backgroundColor: 'red', zIndex: 1, position: 'absolute', margin: 0, top: `${(((new Date().getHours() + (new Date().getMinutes() / 60))-8)*10)}%`, height: 2}}></div>
                  }

                  {/* Each event in a day */}
                  {day.events.map((event, index) =>
                    <Event
                      key={event._id}
                      event={event}
                    />
                  )}
                </Segment>
            )}
            </Segment.Group>
          </Segment.Group>
        </div>
    );
  }
}

export default withTracker(props => {

  // Clone date, because its mutable and we don't want to be modifying the crop
  const date = props.date.clone();

  const room = props.room.toString();
  const results = Meteor.subscribe('events');

  /*
    Here is our query. We want to return all events that:
      1. Are in the selected room
        AND
      2. Start after the beginning of the week of the selected date.
        AND
      3. Start before the end of the week of the selected date.
  */
  return {
    events: Events.find({
      $and: [
        {room: room},
        {startTime: {$gte: date.startOf('week').toDate()}},
        {startTime: {$lte: date.endOf('week').toDate()}}
      ]
    }).fetch(),
    loading: !results.ready(),
    currentUser: Meteor.user(),
  };
})(WeekView);
