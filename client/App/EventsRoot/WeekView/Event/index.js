import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import { Segment, Header, Popup, Button, Icon } from 'semantic-ui-react';
import { Events } from '../../../../../imports/api/events.js';

export default class Event extends Component {


  deleteEvent = (eventID, eventClass) => {
    if (confirm('Are you sure you want to delete this event?')) {
      Meteor.call('events.removeOne', eventID, eventClass);
    } else {

    }
  }

  render() {
    const event = this.props.event;

    // Computer percentage from bottom and top based on time of day
    const topPercentagePadding = (((this.props.event.startTime.getHours() + (this.props.event.startTime.getMinutes() / 60))-8)*10);
    var bottomPercentagePadding = (100-(((this.props.event.endTime.getHours() + (this.props.event.endTime.getMinutes() / 60))-8)*10));

    // If the event goes past, 6PM, lets just cut it off
    // there so it doesn't extend our page
    if (bottomPercentagePadding < 0 ){
      bottomPercentagePadding = 0;
    }

    return (
      <Popup
        trigger={
          <Segment raised style={{ width: '100%', backgroundColor: 'rgb(0, 102, 189)', color: 'white', position: 'absolute', margin: 0, top: `${topPercentagePadding}%`, bottom: `${bottomPercentagePadding}%`, display: 'flex', justifyContent: 'center', alignItems: 'center'}} >
            {event.name}
          </Segment>
        }
        flowing
        hoverable
        position="right center"
        inverted
        style={{borderRadius: '.3rem',   opacity: 0.95,   padding: '1.5em'}}
      >
        <Header as='h4' textAlign='center'>{event.name}</Header>
        <p><Icon circular name='map pin' /> Room {event.room}</p>
        <p><Icon circular name='user' /> {event.professor}</p>
        <p><Icon circular name='clock' /> {event.startTime.toLocaleTimeString('en-US',{ hour12: true, hour: "numeric", minute: "numeric"})} - {event.endTime.toLocaleTimeString('en-US',{ hour12: true, hour: "numeric", minute: "numeric"})}</p>
        { Meteor.user() && Meteor.user().role && Meteor.user().role == 'admin' &&
          <Button color='red' content='Delete Event' size="mini" style={{display: 'block', margin: 'auto'}} onClick={() => this.deleteEvent(event._id, event.class)}/>
        }
      </Popup>
    );
  }
}
